********************************************************************************
* Programme       : sqlite.ado                                                 *
* Programmer      : Jonas Ranstam                                              *
* Programmed date : 06.09.2016                                                 *
********************************************************************************

/* sqlite  v1.0.0 JRanstam 06sep2016

Syntax: sqlite data option spec 

Where data, option and spec are one of:

      database use table
      database query syntax
      database export table
      tablename dump file
      
Requirement: tosql.ado
*/

program define sqlite
   version 14      

   local db="`1'"
   local option="`2'"
   local spec="`3'"

   if "`option'"=="export" {
      local table="`spec'"
      qui {
         tosql, table("`table'") cre
         if "`c(os)'"=="Windows" {
            !type `table'.sql >> cre_`table'.sql
         }
         else {
            !cat `table'.sql >> cre_`table'.sql
	 }
         !sqlite3 `db'.sqlite < cre_`table'.sql
         erase cre_`table'.sql
         erase `table'.sql
      }
   }
   else if "`option'"=="dump" {
      local table="`1'"
      local file="`3'"
      qui {
         tosql, table("`table'") cre
         if "`c(os)'"=="Windows" {
            !type `table'.sql >> cre_`table'.sql
         }
         else {
            !cat `table'.sql >> cre_`table'.sql
	 }
         copy cre_`table'.sql `file'
         erase cre_`table'.sql
         erase `table'.sql
      }
   }
   else if "`option'"=="use" {
      local table="`spec'"
      qui {
         !sqlite3 -header -csv `db'.sqlite "select * from `table';" > tmp.csv
      }
      import delimited tmp.csv, clear
      erase tmp.csv 
   } 
   else if "`option'"=="query" {
      local syntax="`spec'"   
      !sqlite3 -header -csv `db'.sqlite "`syntax';" > tmp.csv
      import delimited tmp.csv, clear
      erase tmp.csv 
   }

end



